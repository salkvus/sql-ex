﻿--Найдите класс, имя и страну для кораблей из таблицы Ships, имеющих не менее 10 орудий.
select Ships.class, name, country
from Ships
inner join Classes Cl on Cl.class = Ships.class
where numGuns >= 10