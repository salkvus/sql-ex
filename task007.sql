﻿--Найдите номера моделей и цены всех имеющихся в продаже продуктов (любого типа) производителя B (латинская буква).
select distinct Product.model, price
from Product
	inner join
	(select model, price
	 from PC
	 union all
	 select model, price
	 from Laptop
	 union all
	 select model, price
	 from Printer
	 ) AllProds
	 on AllProds.model = Product.model
	 where maker = 'B'
