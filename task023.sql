﻿--Найдите производителей, которые производили бы как ПК со скоростью не менее 750 МГц, 
--так и ПК-блокноты со скоростью не менее 750 МГц.
--Вывести: Maker
select distinct Prod.maker
from Product Prod
inner join PC on PC.model = Prod.model
inner join (select maker
            from Product Prod
	    inner join Laptop on Laptop.model = Prod.model
            where Laptop.speed >= 750) Laptop on Laptop.maker = Prod.maker
where PC.speed >= 750
