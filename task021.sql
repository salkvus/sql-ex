﻿--Найдите максимальную цену ПК, выпускаемых каждым производителем, у которого есть модели в таблице PC.
--Вывести: maker, максимальная цена.
select maker, MAX(price) price
from Product Prod
inner join PC on PC.model = Prod.model
group by maker