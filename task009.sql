﻿--Найдите производителей ПК с процессором не менее 450 Мгц. Вывести: Maker
select distinct maker
from Product
inner join PC on PC.model = Product.model
where PC.speed >= 450