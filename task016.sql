﻿--Найдите пары моделей PC, имеющих одинаковые скорость и RAM. 
--В результате каждая пара указывается только один раз, т.е. (i,j), но не (j,i), 
--Порядок вывода: модель с большим номером, модель с меньшим номером, скорость и RAM.
select PC1.model model1, PC2.model model2, PC1.speed, PC1.ram
from PC PC1, PC PC2
where PC2.speed = PC1.speed and PC2.ram = PC1.ram
    and PC2.code <> PC1.code and PC2.model <> PC1.model and PC1.model > PC2.model
group by PC1.model, PC2.model, PC1.speed, PC1.ram

