﻿--Найдите производителя, выпускающего ПК, но не ПК-блокноты.
select distinct maker
from Product
where type = 'PC' and NOT maker IN
	(select maker from Product where type = 'Laptop')