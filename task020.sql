﻿--Найдите производителей, выпускающих по меньшей мере три различных модели ПК. 
--Вывести: Maker, число моделей ПК.
select maker, COUNT(model)
from Product
where type = 'PC'
group by maker
having COUNT(model) >= 3
