﻿--Найдите размеры жестких дисков, совпадающих у двух и более PC. Вывести: HD
select PC1.hd hd
from PC PC1
inner join PC PC2 on PC2.hd = PC1.hd and PC1.code <> PC2.code
group by PC1.hd
having COUNT(PC1.hd) >= 2
