﻿--Найдите модели принтеров, имеющих самую высокую цену. Вывести: model, price
select model, Printer.price
from Printer
inner join (select max(price) price from Printer PrMax) Pr on Pr.price = Printer.price