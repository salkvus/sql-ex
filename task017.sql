﻿--Найдите модели ПК-блокнотов, скорость которых меньше скорости каждого из ПК.
--Вывести: type, model, speed
select distinct 'Laptop', model,speed
from Laptop
where Laptop.speed < ALL (select distinct speed from PC)