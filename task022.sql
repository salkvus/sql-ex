﻿--Для каждого значения скорости ПК, превышающего 600 МГц, определите среднюю цену ПК с такой же скоростью. 
--Вывести: speed, средняя цена.
select speed, AVG(price) price
from PC PC1
where speed > 600
group by speed