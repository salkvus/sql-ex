--Для каждого производителя, выпускающего ПК-блокноты c объёмом жесткого диска не менее 10 Гбайт, найти скорости таких ПК-блокнотов. 
--Вывод: производитель, скорость.
select distinct maker, speed
from Product
inner join Laptop on Laptop.model = Product.model
where Product.type = 'Laptop' and Laptop.hd >= 10
