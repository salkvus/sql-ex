﻿--Найдите среднюю скорость ПК, выпущенных производителем A.
select AVG(speed)
from PC
inner join Product Pr on Pr.model = PC.model
where Pr.maker = 'A'